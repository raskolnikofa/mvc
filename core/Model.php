<?php
class Model{

public $db;

public function __construct(){
try
{
    $db = new PDO('mysql:host=localhost;dbname=blog',
        'root','1234');
    $db->exec('SET NAMES "utf8"');
    $db->setAttribute(PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION);
    $this->db = $db;
}catch (Exception $e)
{
    echo 'Mistake:'.$e->getMessage();
}}

}