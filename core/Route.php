<?php
class Route{
	static function start(){
		$controllerName = 'Main';
		$actionName = 'index';
		$showName = 'NULL';

		$routes = explode('/', $_SERVER['REQUEST_URI']);

		if(!empty($routes[1])){
			$controllerName = mb_convert_case($routes[1], MB_CASE_TITLE);
		}
		if(!empty($routes[2])){
			$actionName = $routes[2];
		}
	
		if(!empty($routes[3])){
			$showName = $routes[3];
		}


		//добавляем префиксы

		$modelName = 'Model_'.$controllerName;

		$controllerName = 'Controller_'.$controllerName;

		if(!empty($routes[3])){
			$actionName = 'action_showId';
		}else{
		$actionName = 'action_'.$actionName;}

		$modelFile = $modelName. '.php';

		if(file_exists('models/'.$modelFile)){

			include 'models/'.$modelFile;

		}

		$controllerFile = $controllerName.'.php';
		if(file_exists('controllers/'.$controllerFile)){
			include 'controllers/'.$controllerFile;
		}else{
			Route::ErrorPage404();
		}

		//new Controller_Book;
		$controller = new $controllerName;
		//action_index 
		$action = $actionName;

		if(method_exists($controller, $action)){
			//$controller->action_index();
			$controller -> $action();
		}else{
			Route::ErrorPage404();
		}

	}
	public static function ErrorPage404(){
		echo "Page not found";
		die();
	}
}