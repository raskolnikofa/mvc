<?php
class Controller_Portfolio extends Controller{

	public function __construct(){
		parent::__construct();
		$this->model = new Model_Portfolio();
	}

		public function action_show(){
		$data = $this->model->getPortfolio();
		$this->view->generate('portfolio.view.php', $data);
	}

		public function action_showId(){
		$data = $this->model->getId();
		$this->view->generate('one_portfolio.view.php', $data);
	}

}