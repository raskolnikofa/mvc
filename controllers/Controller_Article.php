<?php
class Controller_Article extends Controller{

	public function __construct(){
		parent::__construct();
		$this->model = new Model_Article();
	}

		public function action_show(){
		$data = $this->model->getArticle();
		$this->view->generate('article.view.php', $data);
	}

		public function action_showId(){
		$data = $this->model->getId();
		$this->view->generate('one_article.view.php', $data);
	}

}